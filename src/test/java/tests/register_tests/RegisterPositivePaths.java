package tests.register_tests;

import org.testng.annotations.Test;
import page_object.General;
import page_object.LoginPage;
import page_object.RegisterPage;
import tests.BaseTest;


public class RegisterPositivePaths extends BaseTest {
    RegisterPage registerPage = new RegisterPage(driver);
    LoginPage loginPage = new LoginPage(driver);
    General general = new General();

    @Test
    public void successfulRegistration(){

        registerPage
                .inputUsername(username)
                .inputEmail(email)
                .inputPassword(password)
                .inputConfirmPassword(password)
                .confirmTermsAndConditions()
                .confirmSubscription()
                .createAccount(true)
                .pendingRegisterPage(email);
        general.confirmRegistration(email);
        registerPage.registrationSuccessful()
                    .continueRegistration();
        loginPage.login(username, password);
    }

    @Test
    public void successfulRegistrationWithoutSubscription() {

        RegisterPage registerPage = new RegisterPage(driver);
        registerPage
                .inputUsername(username)
                .inputEmail(email)
                .inputPassword(password)
                .inputConfirmPassword(password)
                .confirmTermsAndConditions()
                .createAccount(true)
                .pendingRegisterPage(email);
        general.confirmRegistration(email);
        registerPage.registrationSuccessful()
                .continueRegistration();
        loginPage.login(username, password);
    }
}