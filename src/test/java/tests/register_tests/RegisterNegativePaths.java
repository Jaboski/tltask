package tests.register_tests;

import org.testng.annotations.Test;
import page_object.RegisterPage;
import tests.BaseTest;


public class RegisterNegativePaths extends BaseTest {

    RegisterPage registerPage = new RegisterPage(driver);

    @Test
    public void registrationWithTooLongAndShortUsername() {

        registerPage
                .inputUsername(tooLongUsername)
                .inputUsername("A")
                .inputEmail(email)
                .inputPassword(password)
                .inputConfirmPassword(password)
                .confirmTermsAndConditions()
                .createAccount(false);
    }

    @Test
    public void registrationWithInvalidUsername() {

        registerPage
                .inputUsername(username + "!@#$")
                .inputEmail(email)
                .inputPassword(password)
                .inputConfirmPassword(password)
                .confirmTermsAndConditions()
                .createAccount(false);
    }

    @Test
    public void registrationWithIncorrectEmail() {
        registerPage
                .inputUsername(username)
                .inputEmail("adam")
                .emailShouldBeInvalid()
                .inputPassword(password)
                .inputConfirmPassword(password)
                .confirmTermsAndConditions()
                .createAccount(false);
    }

    @Test
    public void registrationWithIncorrectPassword() {
        registerPage
                .inputUsername(username)
                .inputEmail(email)
                .inputPassword("adam")
                .inputConfirmPassword("adam")
                .confirmTermsAndConditions()
                .createAccount(false);
    }

    @Test
    public void registrationWithPasswordNotMatched() {
        registerPage
                .inputUsername(username)
                .inputEmail(email)
                .inputPassword(password)
                .inputConfirmPassword(password2)
                .passwordNotMatches()
                .confirmTermsAndConditions()
                .createAccount(false);
    }

    @Test
    public void registrationWithTermsAndConditionsNotAccepted() {
        registerPage
                .inputUsername(username)
                .inputEmail(email)
                .inputPassword(password)
                .inputConfirmPassword(password)
                .confirmTermsAndConditions()
                .createAccount(false)
                .termsAndConditionsNotAccepted();
    }
}
