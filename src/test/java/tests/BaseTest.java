package tests;

import driver_manager.DriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import page_object.General;
import utils.Generator;
import static constant.PageContent.*;


public class BaseTest {

    public WebDriver driver;
    Generator generator = new Generator();
    General general = new General();

    public String password = generator.generatePassword();
    public String password2 = generator.generatePassword();
    public String email = generator.generateEmail();
    public String username = generator.generateName();
    public String tooLongUsername = generator.generateTooLongName();

    @BeforeTest
    public void beforeTests() throws InterruptedException {
        driver = DriverManager.getWebDriver();
        driver.manage().window().maximize();
        general.navigateToMainPage();
        general.navigateToLoginPage();
        general.navigateToRegisterPage();
        String pageTitle = driver.getTitle();
        Assert.assertTrue(pageTitle.contains(registerPageTitle));
    }

    @AfterTest
    public void afterTests() {
        DriverManager.closeDriver();
    }
}
