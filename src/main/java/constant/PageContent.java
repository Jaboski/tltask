package constant;

public final class PageContent {

    public static final String mainPageTitle = "Ethereum (ETH) Blockchain Explorer";
    public static final String loginPageTitle = "Etherscan Login Page";
    public static final String registerPageTitle = "Etherscan Registration Page";
    public static final String pendingAlert = "Your account registration has been submitted and is pending email verification";
    public static String textBlock(String email) {
        String text = "We have sent an email to " + email + " with a link to activate your account. To complete the sign-up process, please click on the confirmation link in the email.\n" +
                "\n" +
                "If you do not receive a confirmation email, please check your spam folder and ensure your spam filters allow emails from noreply@etherscan.io.\n" +
                "\n" +
                "If you need any assistance, please contact us.";
        return text;
    }


}
