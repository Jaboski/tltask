package constant;

public final class ErrorMessages {

    public static final String invalidUsername = "Username is invalid.";
    public static final String invalidEmail = "Please enter a valid email address.";
    public static final String invalidPassword = " Your password must be at least 5 characters long.";
    public static final String passwordNotMatched = "Password does not match, please check again.";
    public static final String termsNotAccepted = "Please accept our Terms and Conditions.";
}