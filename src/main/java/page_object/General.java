package page_object;

import driver_manager.DriverManager;
import mail_config.Mailosaur;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import utils.WaitFor;
import static constant.PageContent.*;


public class General {

    WebDriver driver = DriverManager.getWebDriver();

    public General navigateToMainPage() {
        driver.navigate().to("https://etherscan.io");
        Assert.assertEquals(driver.getTitle(), mainPageTitle);
        return this;
    }

    public General navigateToLoginPage() {
        WebElement login = driver.findElement(By.cssSelector("[href='/login']"));
        WaitFor.waitForElementToBeClickable(login);
        login.click();
        Assert.assertEquals(driver.getTitle(), loginPageTitle);
        return this;
    }

    public General navigateToRegisterPage() {
        WebElement register = driver.findElement(By.cssSelector("[href='/register']"));
        WaitFor.waitForElementToBeClickable(register);
        register.click();
        Assert.assertEquals(driver.getTitle(), registerPageTitle);
        return this;
    }

    public void confirmRegistration(String email) {
        String link = getConfirmationLink(email);
        driver.navigate().to(link);
    }

    public String getConfirmationLink(String email) {
        Mailosaur mail = new Mailosaur();
        return mail.getConfirmationLink(email);
    }
}
