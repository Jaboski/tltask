package page_object;

import driver_manager.DriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.WaitFor;


public class LoginPage {

    @FindBy(id = "ContentPlaceHolder1_txtUserName")
    private WebElement usernameField;

    @FindBy(id = "ContentPlaceHolder1_txtPassword")
    private WebElement passwordField;

    @FindBy(id = "ContentPlaceHolder1_btnLogin")
    private WebElement loginButton;

    @FindBy(id = "ContentPlaceHolder1_AdminSideNavigator_Control_myaccount")
    private WebElement myProfile;


    public LoginPage(WebDriver driver){
        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }

    public LoginPage provideUsername(String username) {
        usernameField.sendKeys(username);
        return this;
    }

    public LoginPage inputPassword(String password) {
        passwordField.sendKeys(password);
        return this;
    }

    public LoginPage clickLogin() {
        loginButton.click();
        return this;
    }

    public void login(String username, String password) {
        provideUsername(username);
        inputPassword(password);
        clickLogin();
        WaitFor.waitForElementToBeVisible(myProfile);
    }
}
