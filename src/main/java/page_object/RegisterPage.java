package page_object;

import driver_manager.DriverManager;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import utils.WaitFor;
import utils.Utilities;
import static constant.ErrorMessages.*;
import static constant.PageContent.*;

import java.util.List;


public class RegisterPage {

    @FindBy(id = "ContentPlaceHolder1_txtUserName")
    private WebElement usernameField;

    @FindBy(id = "ContentPlaceHolder1_txtUserName-error")
    private WebElement usernameError;

    @FindBy(id = "ContentPlaceHolder1_txtEmail")
    private WebElement emailField;

    @FindBy(id = "ContentPlaceHolder1_txtEmail-error")
    private WebElement emailError;

    @FindBy(id = "ContentPlaceHolder1_txtPassword")
    private WebElement passwordField;

    @FindBy(id = "ContentPlaceHolder1_txtPassword-error")
    private WebElement passwordError;

    @FindBy(id = "ContentPlaceHolder1_txtPassword2")
    private WebElement confirmPasswordField;

    @FindBy(id = "ContentPlaceHolder1_txtPassword2-error")
    private WebElement confirmPasswordError;

    @FindBy(xpath = "//*[@id='ContentPlaceHolder1_MyCheckBox']")
    private WebElement confirmTermsCheckbox;

    @FindBy(id = "ctl00$ContentPlaceHolder1$MyCheckBox-error")
    private WebElement confirmTermsError;

    @FindBy(id = "ContentPlaceHolder1_SubscribeNewsletter")
    private WebElement confirmSubscriptionCheckbox;

    @FindBy(id = "ContentPlaceHolder1_btnRegister")
    private WebElement registerButton;

    @FindBy(css = ".alert-info")
    private WebElement registrationSuccessfulAlert;

    @FindBy(css = ".d-block")
    private WebElement pendingRegisterText;

    @FindBy(css = ".fa-envelope")
    private WebElement registrationPendingIcon;

    @FindBy(css = ".alert-success")
    private WebElement registrationSuccess;

    @FindAll(@FindBy(xpath = "//*[contains(@id, '-error')]"))
    private List<WebElement> errors;

    @FindAll(@FindBy(id = "ContentPlaceHolder1_btnContinue"))
    private WebElement continueButton;


    public RegisterPage(WebDriver driver){
        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }

    public RegisterPage inputUsername(String username) {
        if (usernameField.getAttribute("value").chars().count() > 0) {
            usernameField.clear();
        }

        usernameField.sendKeys(username);
        String usernameText = usernameField.getAttribute("value");

        if (usernameText.chars().count() < 5  && usernameText.matches("^[a-zA-Z0-9]*$")) {
            WaitFor.waitForElementToBeVisible(usernameError);
            Utilities.checkText(usernameError, invalidUsername);
        } else if (username.chars().count() > 30) {
            Assert.assertEquals(usernameText.chars().count(),  30);
        } else {
            Assert.assertEquals(usernameText, username);
        }
        return this;
    }

    public RegisterPage inputEmail(String email) {
        emailField.sendKeys(email);
        Assert.assertEquals(emailField.getAttribute("value"), email);
        return this;
    }

    public RegisterPage emailShouldBeInvalid() {
        WaitFor.waitForElementToBeVisible(emailError);
        Utilities.checkText(emailError, invalidEmail);
        return this;
    }

    public RegisterPage inputPassword(String password) {
        passwordField.sendKeys(password);
        if (password.chars().count() < 5 ) {
            WaitFor.waitForElementToBeVisible(passwordError);
            Utilities.checkText(passwordError, invalidPassword);
        }
        return this;
    }

    public RegisterPage inputConfirmPassword(String password) {
        confirmPasswordField.sendKeys(password);
        if (password.chars().count() < 5 ) {
            WaitFor.waitForElementToBeVisible(confirmPasswordError);
        }
        return this;
    }

    public RegisterPage confirmTermsAndConditions() {
        if(!confirmTermsCheckbox.isSelected()) {
            JavascriptExecutor executor = (JavascriptExecutor)DriverManager.getWebDriver();
            executor.executeScript("arguments[0].click();", confirmTermsCheckbox);
        }
        return this;
    }

    public RegisterPage confirmSubscription() {
        JavascriptExecutor executor = (JavascriptExecutor)DriverManager.getWebDriver();
        executor.executeScript("arguments[0].click();", confirmSubscriptionCheckbox);
        return this;
    }

    public RegisterPage createAccount(boolean isSuccessful) {
        registerButton.click();
        if (isSuccessful) {
            WaitFor.waitForElementToBeVisible(registrationSuccessfulAlert);
            WaitFor.waitForElementToBeVisible(registrationPendingIcon);
        } else {
            Assert.assertTrue(errors.size() > 0);
        }
        return this;
    }

    public RegisterPage passwordNotMatches() {
        WaitFor.waitForElementToBeVisible(confirmPasswordError);
        Utilities.checkText(confirmPasswordError, passwordNotMatched);
        return this;
    }

    public RegisterPage termsAndConditionsNotAccepted() {
        WaitFor.waitForElementToBeVisible(confirmTermsError);
        Utilities.checkTextContent(confirmTermsError, termsNotAccepted);
        return this;
    }

    public RegisterPage pendingRegisterPage(String email) {
        WaitFor.waitForElementToBeVisible(registrationSuccessfulAlert);
        String text = textBlock(email);
        Utilities.checkTextContent(registrationSuccessfulAlert, pendingAlert);
        Utilities.checkTextContent(pendingRegisterText, text);
        return this;
    }

    public RegisterPage registrationSuccessful() {
        WaitFor.waitForElementToBeVisible(registrationSuccess);
        return this;
    }

    public void continueRegistration() {
        continueButton.click();
    }

}