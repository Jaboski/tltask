package mail_config;

import org.testng.Assert;
import com.mailosaur.MailosaurClient;
import com.mailosaur.MailosaurException;
import com.mailosaur.models.*;

import java.io.IOException;
import static mail_config.MailConf.*;


public class Mailosaur {

    public String getConfirmationLink(String email) {

        Message message;

        MailosaurClient mailosaur = new MailosaurClient(apiKey);

        SearchCriteria criteria = new SearchCriteria();
        criteria.withSentTo(email);

        try {
            message = mailosaur.messages().get(serverId, criteria);
        } catch(IOException | MailosaurException ex) {
            message = null;
        }

        Assert.assertNotNull(message);
        Assert.assertEquals("Please confirm your email [Etherscan.io]", message.subject());
        String mailBody = message.text().body();
        String[] link = mailBody.split("Confirmation Link: ");
        link = link[1].split(" ");
        return link[0];
    }
}

