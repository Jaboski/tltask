package utils;

import org.openqa.selenium.WebElement;
import org.testng.Assert;


public class Utilities {

    public static String getElementTextByValue(WebElement element) {
        return element.getAttribute("value");
    }

    public static String getElementText(WebElement element) {
        return element.getText();
    }

    public static void checkTextContent(WebElement element, String errorMessage){
        Assert.assertEquals(getElementTextByValue(element), errorMessage);
    }

    public static void checkText(WebElement element, String errorMessage){
        Assert.assertEquals(getElementText(element), errorMessage);
    }

}
