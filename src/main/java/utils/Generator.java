package utils;

import com.github.javafaker.Faker;
import static mail_config.MailConf.serverDomain;


public class Generator {

    public Faker faker = new Faker();

    public String generateName() {
        String fName = faker.name().firstName();
        String lName = faker.name().lastName();
        return fName + lName + "1";
    }

    public String generateTooLongName() {
        String fName = faker.name().firstName();
        String lName = faker.name().lastName();
        return fName + lName + fName + lName + fName + lName + fName + lName;
    }

    public String generateEmail() {

        String fName = faker.name().firstName();
        String lName = faker.name().lastName();
        return fName + lName + "@" + serverDomain;
    }

    public String generatePassword() {
        String color = faker.name().lastName();
        String number = faker.phoneNumber().cellPhone().toString().replace(".", "");
        return color + number + "!@#$%^&*()";
    }
}
