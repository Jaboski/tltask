package utils;

import driver_manager.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class WaitFor {

    private static WebDriverWait getWebDriverWait(){
        return new WebDriverWait(DriverManager.getWebDriver(),10);
    }

    public static void waitForElementToBeVisible(WebElement element){
        getWebDriverWait().until(ExpectedConditions.visibilityOf(element));
    }

    public static void waitForElementToBeClickable(WebElement element){
        getWebDriverWait().until(ExpectedConditions.elementToBeClickable(element));
    }
}
